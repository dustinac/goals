# Big
- [ ] acquire race vehicle - second hand 1600
- [ ] purchase first residence (west coast)

## Personal

- [ ] re-establish consistent work-out regimen
- [ ] establish local friend group
- [ ] participate in an ongoing local community effort (eg joining a group, volunteering)
- [ ] establish photo/video posting regimen
    - [ ] truck build stuff
    - [ ] travel/exploring stuff
- [ ] leave mainstream social media
    - [ ] improve personal matrix instance infrastructure
    - [ ] complete matrix instance bridge deployment

## Relationship

- [ ] Dedicated time to relationship work
- [ ] Explicit mindfulness regarding communication/style
- [ ] Improved ability to ask for help

## Professional

- [ ] grow tdd opportunities/portfolio
- [ ] establish studio w/ named, physical presence
- [ ] financial goal 100
- [ ] 4wd.build launch
- [ ] vizn launch
- gt
    - [ ] full technology assessment
    - [ ] build team
    - [ ] create and implement robust three tier model 

## Housing

- [X] narrow in on preferred location(s)
- [/] research area(s)
    - [/] remote
    - [/] on the ground
- [ ] make offer
- [ ] purchase first residence
- [ ] establish residency

## Travel

- [ ] explore northern nh
- [ ] explore (northern) nevada

## Racing

- [X] attend first off-road race
- [X] first desert driving/pre-running experience
- [ ] acquire a manual trans vehicle
- [ ] manual trans training
- [X] simulator build
- [X] choose a class
- [ ] first race entry
- [ ] establish training regimen
    - [X] simulator
    - [ ] on track

## Truck build

- [X] wheels and tires
- [X] auxiliary power system
- [X] solar power system
- [ ] underbody skid plates
- [X] compressor and air system
- [X] communications systems
    - [X] gmrs
    - [X] cell
    - [X] wifi/wan
- [ ] re-gearing
    - [X] front and rear 4.10
    - [ ] ecm/tcm tuning
- [ ] bed rack
    - [X] rack install
    - [ ] drawer/storage system
- [ ] hc rear bumper
    - [ ] main bumper
    - [ ] swing-out/tire carrier
- [ ] supercharger
- [ ] front suspension
    - [ ] upper/lower arm kit
    - [ ] extended axle shafts
    - [ ] extended travel coilovers
    - [ ] 3 tube bypass
        - [ ] additional mount
- [ ] rear suspension
    - [ ] extended travel 3 tube bypass (or remote) 
    - [ ] leaf springs
    - [ ] mounts
- [ ] roof top tent